
var gulp = require("gulp");
var fileinclude = require('gulp-file-include');
var svgSprite = require("gulp-svg-sprites");
//var tinypng = require('gulp-tinypng');
var spritesmith = require('gulp.spritesmith');


gulp.task('svg-icons', function () {
    return gulp.src('assets/sprites-svg/*.svg')
        .pipe(svgSprite(
        	{
        		mode: "symbols",

        	},
        	{
            svg: {
                sprite: "sprite-svg.svg"
	            }
	        }

        	))
        .pipe(gulp.dest("dev"));
});

gulp.task('includes', function() {
  gulp.src('./dev/*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./'));
});




// Sprites task - create sprite image
gulp.task('sprite', function () {
  var spriteData = gulp.src('assets/icon-img/*.png').pipe(spritesmith({
    imgName: 'sprite-img.png',
    cssName: 'sprite.css',
    imgPath: '../assets/sprite-img.png'
  }));
  //return spriteData.pipe(gulp.dest('path/to/output/'));
    spriteData.css.pipe(gulp.dest('css'));
    spriteData.img.pipe(gulp.dest('assets'));


});






