
$(function() {
	//related products slider. 4 prods version
		$(".prods-slider").owlCarousel(
			{
				 margin:20,
				 items:4

			});
		$('body').scrollspy({target: ".toc", offset: 70});
		//animate scroll to anchor
		var $root = $('html, body');
		$('.toc a[href^="#"]').click(function () {
		    $root.animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top-50
		    }, 500);

			    return false;


		});


		//zoom product page
		$('.xzoom, .xzoom-gallery').xzoom();







});
