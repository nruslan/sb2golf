<div id="full-specs">
    <h3>The Preserve Golf Course</h3>
    <div class="table-specs responsive-table">
        <table class="table">
            <thead class="bg-dark">
            <tr>
                <th scope="col">Tee</th>
                <th scope="col">Par</th>
                <th scope="col">Yards</th>
                <th scope="col">Rating</th>
                <th scope="col">Slope</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Black</th>
                <td>72</td>
                <td>7,006</td>
                <td>72.4</td>
                <td>139</td>
            </tr>
            <tr>
                <th scope="row">Purple</th>
                <td>72</td>
                <td>6,418</td>
                <td>69.9</td>
                <td>130</td>
            </tr>
            <tr>
                <th scope="row">Green</th>
                <td>72</td>
                <td>5,495</td>
                <td>65.8</td>
                <td>119</td>
            </tr>
            <tr>
                <th scope="row">Red</th>
                <td>72</td>
                <td>4,920</td>
                <td>69.3</td>
                <td>120</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>