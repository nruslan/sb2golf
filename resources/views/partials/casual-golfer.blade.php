<section class="regular-spacing-section">
    <div class="container">
        <div class="lead col-10 col-sm-8 mx-auto">
            <p class="text-justify">If you only play later in the day or on a limited basis, this may be the deal for you! <strong>For just $269, you receive:</strong></p>
        </div>
        <div class="row">
            <div class="lead col-8 col-sm-6 mx-auto">
                <ul>
                    <li>(3) 18-Hole Rounds OR (6) 9-Hole Rounds of Golf to be used at <strong>ANY</strong> time.</li>
                    <li><strong>Tee Time Priority</strong> comparable to a Punch Card Player.</li>
                    <li><strong>Discounted Afternoon Rates</strong> throughout the year, as stated below:</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="lead col-10 col-sm-8 mx-auto">
                <h2 class="section-head text-center">Play Card Fees</h2>
                <div class="table-specs responsive-table">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Dates</th>
                            <th scope="col">After 1:30 PM</th>
                            <th scope="col">After 3:00 PM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">12/1/19 – 3/31/20</th>
                            <td>$35</td>
                            <td>$19</td>
                        </tr>
                        <tr>
                            <th scope="row">4/1/20 – 5/31/20</th>
                            <td>$32</td>
                            <td>$19</td>
                        </tr>
                        <tr>
                            <th scope="row">6/1/20 – 9/30/20</th>
                            <td>$23</td>
                            <td>$15</td>
                        </tr>
                        <tr>
                            <th scope="row">10/1/20 – 11/30/20</th>
                            <td>$32</td>
                            <td>$19</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>