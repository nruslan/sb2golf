<section class="regular-spacing-section">
    <div class="container">
        <div class="lead col-10 col-sm-8 mx-auto">
            <p class="text-justify">Combined MountainView & The Preserve Golf Club Annual Passes are available for purchase. Annual golf pass holders do not pay greens fees for such pass holder’s golf play during {{ $dateRange }}</p>
            <h2 class="section-head text-center">ANNUAL PASS FEES</h2>
            <div class="table-specs responsive-table">
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">1st  Person in Household</th>
                        <td>$4,110 (Base Fee)</td>
                        <td>$3,931 (Cash Price)</td>
                    </tr>
                    <tr>
                        <th scope="row">2nd  Person in Household</th>
                        <td>$3,790 (Base Fee)</td>
                        <td>$3,716 (Cash Price)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h2 class="section-head text-center">MountainView/Preserve Annual Pass Includes:</h2>
        <div class="lead col-6 col-sm-4 mx-auto">
            <ul>
                <li>20% discount on in-stock soft goods</li>
                <li>10% discount on in-stock hard goods</li>
                <li>50-bucket Range Card ($170.00 Value)</li>
                <li>(2) Guest Passes per Annual Pass for guests from outside SaddleBrooke</li>
                <li>Annual Merchandise Sale for Annual Passholders only</li>
                <li>Golf Improvement fee of $50 included in fee above</li>
            </ul>
        </div>

        <div class="lead col-10 col-sm-8 mx-auto">
            <h2 class="section-head text-center">AFTERNOON ANNUAL PASS FEES</h2>
            <div class="table-specs responsive-table">
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">PM Pass Fees per Player</th>
                        <td>$2,700 (Base Fee)</td>
                        <td>$2,648 (Cash Price)</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <h2 class="section-head text-center">MountainView/Preserve Afternoon Annual Pass Includes:</h2>
        <div class="lead col-6 col-sm-4 mx-auto">
            <ul>
                <li>Play any time after 12:00 PM</li>
                <li>10% discount on in-stock soft goods</li>
                <li>25-bucket Range Card ($85.00 Value)</li>
                <li>(1) Guest Pass per Annual Pass for guests from outside SaddleBrooke</li>
                <li>Annual Merchandise Sale for Annual Passholders only</li>
                <li>Golf Improvement fee of $50 included in fee above</li>
            </ul>
        </div>
    </div>
</section>