<section class="regular-spacing-section">
    <div class="container">
        <div class="lead col-10 col-sm-8 mx-auto">
            <p class="text-justify">Combined MountainView & The Preserve Golf Club Play Cards may be purchase as well. Play card holders may use one play for greens fees for each round of golf play during {{ $dateRange }} until all plays are used.</p>
            <h2 class="section-head text-center">Play Card Fees</h2>
            <div class="table-specs responsive-table">
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">MV/PV 50-Play Card (18-Holes)</th>
                        <td>$2,825 (Base Fee)</td>
                        <td>$2,770 (Cash Price)</td>
                    </tr>
                    <tr>
                        <th scope="row">MV/PV 25-Play Card (18-Holes)</th>
                        <td>$1,550.50 (Base Fee)</td>
                        <td>$1,520.50 (Cash Price)</td>
                    </tr>
                    <tr>
                        <th scope="row">MV/PV 50-Play Card (9-Holes)</th>
                        <td>$1,588 (Base Fee)</td>
                        <td>$1,558 (Cash Price)</td>
                    </tr>
                    <tr>
                        <th scope="row">MV/PV 25-Play Card (9-Holes)</th>
                        <td>$873 (Base Fee)</td>
                        <td>$858 (Cash Price)</td>
                    </tr>
                    <tr>
                        <th scope="row">MV/PV 50-Play PM Card ***</th>
                        <td>$2,008 (Base Fee)</td>
                        <td>$1,970 (Cash Price)</td>
                    </tr>
                    </tbody>
                </table>
                <p><em><small>* Play Cards can be shared within the household and used for non-resident guests. Golf Improvement fee of $75 for 50-Play cards and $37.50 for 25-Play Cards included in fee above.<br/> ***Use after 12:00 p.m. any day of week.</small></em></p>
            </div>
        </div>
    </div>
</section>