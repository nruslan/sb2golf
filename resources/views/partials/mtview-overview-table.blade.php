<div id="full-specs">
    <h3>MountainView Golf Course</h3>
    <div class="table-specs responsive-table">
        <table class="table">
            <thead class="bg-dark">
            <tr>
                <th scope="col">Tee</th>
                <th scope="col">Par</th>
                <th scope="col">Yards</th>
                <th scope="col">Rating</th>
                <th scope="col">Slope</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Black</th>
                <td>72</td>
                <td>6,728</td>
                <td>71.3</td>
                <td>127</td>
            </tr>
            <tr>
                <th scope="row">Purple</th>
                <td>72</td>
                <td>6,040</td>
                <td>68.1</td>
                <td>118</td>
            </tr>
            <tr>
                <th scope="row">Green</th>
                <td>72</td>
                <td>5,545</td>
                <td>65.1</td>
                <td>107</td>
            </tr>
            <tr>
                <th scope="row">Red</th>
                <td>72</td>
                <td>5,030</td>
                <td>68.1</td>
                <td>114</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>