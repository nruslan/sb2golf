@extends('layouts.auth.app')

@section('title', 'Admin Login')
@section('class', "class=login-page")

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('index') }}">
            <img src="{{ asset('images/sb2golf-logo.png') }}" alt="SB2 Golf Logotype" width="100"><br>
            <b>SB2</b>Golf</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form method="POST" action="{{ route('login') }}" id="login-form">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                    <div class="input-group-append">
                        <span class="fa fa-envelope input-group-text"></span>
                    </div>
                    @if ($errors->has('email'))<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>@endif
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                    <div class="input-group-append">
                        <span class="fa fa-lock input-group-text"></span>
                    </div>
                    @if ($errors->has('password'))<span class="invalid-feedback" role="alert"><strong>{{ $errors->first('password') }}</strong></span>@endif
                </div>
                <div class="row">
                    <div class="col-7">
                        <div class="icheck-primary">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Remember Me</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-5">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In <i class="fas fa-sign-in-alt fa-fw"></i></button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <p class="mb-1">
                <a href="#">I forgot my password</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
@endsection
@section('script')
<script>
    $(function () {
        $("#login-form").submit(function() {
            $(this).closest('form').find(':submit').prop('disabled',true).children("i.fas").replaceWith('<i class="fas fa-spinner fa-pulse fa-fw"></i>');
        });
    })
</script>
@endsection
