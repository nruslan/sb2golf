@component('mail::message')
# The Preserve Golf Club Contact Form

+ Message from: {{ $name }}
+ Email: {{ $email }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [PreserveGolf.Club](https://preservegolf.club) website contact form._
@endcomponent