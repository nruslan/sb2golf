@component('mail::message')
# The Preserve Golf Club Outings Inquiry Form

+ Message from: {{ $name }}
+ Email: {{ $email }}
+ Phone: {{ $phone }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [PreserveGolf.Club](https://preservegolf.club) website outings inquiry form._
@endcomponent