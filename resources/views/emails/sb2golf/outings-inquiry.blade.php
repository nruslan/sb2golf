@component('mail::message')
# SaddleBrooke Golf Outings Inquiry Form

+ Message from: {{ $name }}
+ Email: {{ $email }}
+ Phone: {{ $phone }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [SaddleBrookeGolf.com](https://saddlebrookegolf.com) website outings inquiry form._
@endcomponent