@component('mail::message')
# SaddleBrooke Golf Contact Form

+ Message from: {{ $name }}
+ Email: {{ $email }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [SaddleBrookeGolf.com](https://saddlebrookegolf.com) website contact form._
@endcomponent