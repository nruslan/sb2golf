@component('mail::message')
# MountainView Golf Club Outings Inquiry Form

+ Message from: {{ $name }}
+ Email: {{ $email }}
+ Phone: {{ $phone }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [MountainViewGolf.Club](https://mountainviewgolf.club) website outings inquiry form._
@endcomponent