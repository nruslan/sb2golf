@component('mail::message')
# MountainView Golf Club Contact Form

+ Message from: {{ $name }}
+ Email: {{ $email }}

{{ $bodyMessage }}

@component('mail::button', ['url' => "mailto:$email"])
Reply to {{ $name }}
@endcomponent

---
_This email has been sent from [MountainViewGolf.Club](https://mountainviewgolf.club) website contact form._
@endcomponent