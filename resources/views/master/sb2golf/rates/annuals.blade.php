@extends('layouts.app')

@section('title', 'Annual Membership')

@section('content')
    <div class="container">
        <div class="container">
            <!-- title -->
            <div class="page-title regular-spacing-section col-12">
                <h1>Annuals</h1>
            </div>
            <!-- /title -->
        </div>
        @include('partials.annuals')
        <div class="spacer"></div>
    </div>
@endsection