<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ruslan Niyazimbetov">

    <title>SaddleBrooke TWO Golf</title>

    <!-- Bootstrap core CSS -->
    <!-- Styles -->
    <link href="{{ asset('css/coming-soon.css') }}" rel="stylesheet">

</head>

<body>

<div class="overlay"></div>
<video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="{{ asset("storage/sb2golf.mp4") }}" type="video/mp4">
</video>

<div class="masthead">
    <div class="masthead-bg"></div>
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-12 my-auto">
                <div class="masthead-content text-white py-5 py-md-0">
                    <h1 class="mb-3">Sorry! We're Not Ready Yet.</h1>
                    <p class="mb-5">Please visit <a href="http://sbhoa2.org/golf" class="txt-white">sbhoa2.org/golf</a> for tee times.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="social-icons">
    <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
            <a href="https://mountainviewgolf.club/" title="MountainView Golf Club">
                <i class="fas fa-globe"></i>
            </a>
        </li>
    </ul>
</div>

<!-- Scripts -->
<script src="{{ asset('js/coming-soon.js') }}"></script>

</body>

</html>
