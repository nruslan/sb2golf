@extends('layouts.app')

@section('title', 'SaddleBrooke')

@section('slider')
<div id="carouselExampleIndicators" class="carousel slide lazy" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active text-center">
            <picture>
                <source srcset="{{ asset("storage/images/slider/pr-slider-1-sm.jpg") }}" media="(max-width: 768px)">
                <source srcset="{{ asset("storage/images/slider/pr-slider-1-md.jpg") }}" media="(max-width: 1124px)">
                <source srcset="{{ asset("storage/images/slider/pr-slider-1-lg.jpg") }}">
                <img src="{{ asset("storage/images/slider/pr-slider-1-lowres.jpg") }}" class="img-fluid" alt="First slide">
            </picture>
        </div>
        <div class="carousel-item text-center">
            <picture>
                <source srcset="{{ asset("storage/images/slider/mv-slider-2-sm.jpg") }}" media="(max-width: 768px)">
                <source srcset="{{ asset("storage/images/slider/mv-slider-2-md.jpg") }}" media="(max-width: 1124px)">
                <source srcset="{{ asset("storage/images/slider/mv-slider-2-lg.jpg") }}">
                <img src="{{ asset("storage/images/slider/mv-slider-2-lowres.jpg") }}" class="img-fluid" alt="Second slide">
            </picture>
        </div>
        <div class="carousel-item text-center">
            <picture>
                <source srcset="{{ asset("storage/images/slider/mv-slider-1-sm.jpg") }}" media="(max-width: 768px)">
                <source srcset="{{ asset("storage/images/slider/mv-slider-1-md.jpg") }}" media="(max-width: 1124px)">
                <source srcset="{{ asset("storage/images/slider/mv-slider-1-lg.jpg") }}">
                <img src="{{ asset("storage/images/slider/mv-slider-1-lowres.jpg") }}" class="img-fluid" alt="First slide">
            </picture>
        </div>
    </div>
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@endsection

@section('content')
<!-- services -->
{{-- <section class="services py-2 regular-spacing-section bg-dark">
    <div class="container">
        <div class="row d-flex justify-content-around itext">
            <!-- service 1 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_tel"></use>
                    </svg>
                </div>
                <div>
                    <strong>Call Us {{ $phone }}</strong>
                </div>
            </div>
            <!-- /service  -->
            <!-- service 2 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_clock"></use>
                    </svg>
                </div>
                <div>
                    <strong>Hours 7AM - 5PM</strong>
                </div>
            </div>
            <!-- /service  -->
        </div>
    </div>
</section> --}}
<!-- /services -->

<!-- Welcome Section -->

<section class="top-cat regular-spacing-section">
    <div class="container">
        <h2 class="section-head text-center">Welcome to SaddleBrooke TWO Golf</h2>
        <br>
        <div class="row no-gutters">
            <!-- cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/sb2-MountainView.jpg") }}" alt="Clothing"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('golf.mountainview') }}" onClick="ga('send', 'event', 'Outbound Links', 'Clicks', 'Tee Times Home Page')"><small>Golf</small><strong>MountainView</strong></a></h3>
                    </div>
                </div>
            </div>
            <!-- /cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/sb2-Preserve.jpg") }}" alt="Accesories"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('golf.preserve') }}"><small>Golf</small><strong>The Preserve</strong></a></h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/sb2-Group-Outings.jpg") }}" alt="Card image"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('group.outings') }}"><small>Group</small><strong>OUTINGS</strong></a></h3>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</section>
<!-- /Welcome Section -->

<!-- NEWS Vue Component -->
<news-component></news-component>
<!-- NEWS Vue Component  -->

<!-- TEXT CONTENT -->
<section class="regular-spacing-section bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="section-head text-center" id="prod-desc">Make Your Story to Tell</h2>
            </div>
            <div class="col-md-9 mx-auto lead text-center">
                <p>With elevated tees and holes amidst dramatic boulders, SaddleBrooke TWO's MountainView Golf Course presents an enjoyable challenge to novices as well as avid golfers, offering a fascinating layout and majestic mountain views. Winding through the beautiful canyons and foothills of the Santa Catalina Mountains, The Preserve Golf Club 18-hole Championship Course gives you the feeling that you’re literally on top of the world. Located at a higher elevation than most other golf clubs in Tucson, both courses are a terrific choice for your regular rounds of golf, or as a new destination for you and friends when visiting Arizona. Make <i>Your</i> Story to Tell at MountainView and Preserve Golf Clubs!</p>
                <br>
            </div>
        </div>
    </div>
</section>
<!-- /TEXT CONTENT -->

<div class="regular-spacing-section banners">
    <div class="container">
            <br>
        <div class="row">
            <div class="col-sm-6 text-center">
                @include('partials.mtview-overview-table')
            </div>
            <div class="col-sm-6 text-center">
                @include('partials.preserve-overview-table')
            </div>
        </div>
    </div>
</div>
@endsection