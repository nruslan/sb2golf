@extends('layouts.app')

@section('title', 'Facilities')

@section('content')
<div class="container">
    <div class="container">
        <!-- title -->
        <div class="page-title regular-spacing-section col-12">
            <h1>Facilities</h1>
        </div>
        <!-- /title -->
    </div>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Preserve-Golf-Shop.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Golf Shops</h2>
                    <p>Grab your logo apparel and the latest golf equipment while visiting our well-stocked MountainView and Preserve Golf Shops. Featuring the best from Callaway, Cobra, Cleveland, Nike, Under Armour, and more. Golf Shops are open 7 AM to 5 PM daily.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Practice-Facilities.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-12 col-lg-7 text-left px-1 px-md-5">
                    <h2>Practice Facilities</h2>
                    <p>Both courses feature a large putting green and a short game practice area. MountainView also offers a full scale driving range, and The Preserve offers a practice tee. Our instructional programming covers all parts of the game. Individual, group, and playing lessons are available from our PGA Golf Professionals.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Preserve-Restaurant.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Preserve Restaurant & Bar</h2>
                    <p>Experience a taste of excellence at The Preserve after a challenging day on the links! A gem among restaurants with dining as spectacular as the views, The Preserve boasts a distinctive menu of Modern American Cuisine. Regular hours are 11 AM to 8 PM, Wednesday – Sunday. Closed Monday and Tuesday.</p>
                </div>
            </div>
        </div>
    </section>
    
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Mesquite-Grill.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-12 col-lg-7 text-left px-1 px-md-5">
                    <h2>Mesquite Grill</h2>
                    <p>Drop by the Grill after a great day on the links! Serving some of the best diner food around for breakfast and lunch. Regular hours are 7 AM to 2 PM Monday – Saturday, and 8 AM to 2 PM on Sundays (serving breakfast all day).</p>
                </div>
            </div>
        </div>
    </section>
    
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/The-Bistro.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>MountainView Bar & Grill</h2>
                    <p>Ready to grab a relaxing drink at the bar? Starving for a hearty dinner? MVBG is just around the corner! Join us every Tuesday through Saturday. Happy Hour is 3 PM – 6 PM. Dinner is served 4:30 PM – 8 PM.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="spacer"></div>
</div>
@endsection