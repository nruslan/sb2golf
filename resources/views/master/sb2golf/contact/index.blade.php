@extends('layouts.app')

@section('title', 'Contact Us')

@section('content')
    <!-- google map load by script-->
    <div id="map">
        <div id="company-location"></div>
    </div>
    <!-- /google map-->
    <div class="container">
        <section id="contact">
            <div class="container">
                <!-- title -->
                <div class="page-title regular-spacing-section col-12">
                    <h1>Contact Us</h1>
                    <p class="lead">with questions or group event inquiries</p>
                </div>
                <!-- /title -->

                <div class="row">
                    <div class="col-md-8 regular-spacing-section">
                        <!-- form -->
                        <contact-us-component></contact-us-component>

                        <!-- /form -->
                    </div>
                    <div class="col-md-3 ml-auto">
                        <ul class="list-unstyled address-list">
                            <li><svg class="icon small"><use xlink:href="#icon_location"></use></svg> 66567 East Catalina Hills Drive<br>SaddleBrooke, Tucson AZ.</li>
                            <li><svg class="icon small"><use xlink:href="#icon_email"></use></svg> {{ eMail::mutate($email) }}</li>
                            <li><svg class="icon small"><use xlink:href="#icon_tel"></use></svg> {{ $phone }}</li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <div class="spacer"></div>
    </div>
@endsection

@section('script')
<script>
    var map;
    function initMap() {
        var location_center = {lat:32.53867250014395, lng:-110.87444195365907 };
        
        map = new google.maps.Map(document.getElementById('company-location'), {
            center: location_center,
            zoom: 14,
            disableDefaultUI: true,
                styles: [{
                    stylers: [{
                        saturation: -100 //grayscale map
                    }]
                }]
        });
    
        var locations = [
            {
                title: "MountainView Golf Club",
                position: new google.maps.LatLng(32.52536956485852, -110.89779728865625),
                type: 'info',
                image: "images/assets/map-marker.png", //custom marker
                contentString: '<div id="content">'+
                    '<div id="siteNotice">'+
                    '</div>'+
                    '<h1 id="firstHeading" class="firstHeading">MountainView Golf Club</h1>'+
                    '<div id="bodyContent">'+
                    '<p>Set in the foothills of the Catalina Mountains, MountainView Golf Club in Tucson, Arizona is an 18-hole golf course that lives up to its name with magnificent views of the surrounding mountains. Designed by Gary Panks, the course weaves through a hilly, natural desert landscape to create a fascinating layout. The wide-open design often makes for windy conditions, but Panks provides golfers with generous landing areas in the fairways. Mountainview Golf Club is a great track for low and high handicappers alike to enjoy a terrific day on the course.</p>'+
                    '<p>Website: <a href="https://mountainviewgolf.club/">MountainViewGolf.club</a> '+
                    '</div>'+
                    '</div>'
            }, {
                title: "The Preserve Golf Club",
                position: new google.maps.LatLng(32.54960915640782, -110.85516089415546),
                type: 'info',
                image: "images/assets/map-marker.png", //custom marker
                contentString: '<div id="content">'+
                    '<div id="siteNotice">'+
                    '</div>'+
                    '<h1 id="firstHeading" class="firstHeading">The Preserve Golf Club</h1>'+
                    '<div id="bodyContent">'+
                    '<p>Winding through the beautiful canyons and foothills of the Santa Catalina Mountains, The Preserve Golf Club 18-hole Championship Course gives you the feeling that you’re literally on top of the world. The layout features rolling fairways and undulating greens, presenting an enjoyable challenge to novices as well as avid golfers. Boasting excellent views and dramatic elevation changes, The Preserve is a terrific choice for your regular rounds of golf, or as a new destination for you and friends when visiting Tucson.</p>'+
                    '<p>Website: <a href="https://preservegolf.club/">PreserveGolf.club</a> '+
                    '</div>'+
                    '</div>'
            }
        ];

        // Create markers.
        locations.forEach(function(loc) {

            var marker = new google.maps.Marker({
                position: loc.position,
                icon: loc.image,
                map: map,
                title: loc.title
            });
            
            var infowindow = new google.maps.InfoWindow({
                content: loc.contentString
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        });
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgayCTFaZgMo1ucPvPeaafUupX8eyKTKU&callback=initMap" async defer></script>
@endsection