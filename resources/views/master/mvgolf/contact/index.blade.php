@extends('layouts.mvgolf')

@section('title', 'Contact Us')

@section('content')
    <!-- google map load by script-->
    <div id="map">
        <div id="company-location"></div>
    </div>
    <!-- /google map-->
    <div class="container">
        <section id="contact">
            <div class="container">
                <!-- title -->
                <div class="page-title regular-spacing-section col-12">
                    <h1>Contact Us</h1>
                    <p class="lead">with questions or group event inquiries</p>
                </div>
                <!-- /title -->

                <div class="row">
                    <div class="col-md-8 regular-spacing-section">
                        <!-- form -->
                        <contact-us-component></contact-us-component>

                        <!-- /form -->
                    </div>
                    <div class="col-md-3 ml-auto">
                        <ul class="list-unstyled address-list">
                            <li><svg class="icon small"><use xlink:href="#icon_location"></use></svg> 38691 S MountainView Blvd, SaddleBrooke, Tucson AZ.</li>
                            <li><svg class="icon small"><use xlink:href="#icon_email"></use></svg> {{ eMail::mutate($email) }}</li>
                            <li><svg class="icon small"><use xlink:href="#icon_tel"></use></svg> {{ $phone }}</li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <div class="spacer"></div>
    </div>
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgayCTFaZgMo1ucPvPeaafUupX8eyKTKU&callback=initMap"></script>
    <script>
        //mapa
        function init_map()
        {
            var var_location = new google.maps.LatLng(32.52536956485852,-110.89779728865625); //position
            var var_mapoptions = {
                center: var_location,
                zoom: 18,
                disableDefaultUI: true,
                styles: [{
                    stylers: [{
                        saturation: -100 //grayscale map
                    }]
                }]
            };
            var image = "images/assets/map-marker.png"; //custom marker
            var var_marker = new google.maps.Marker({
                position: var_location,
                map: var_map,
                icon: image,
                title:"MountainView Golf Club"});

            var var_map = new google.maps.Map(document.getElementById("company-location"),
                var_mapoptions);

            var_marker.setMap(var_map);

        }

        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
@endsection