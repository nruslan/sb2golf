@extends('layouts.mvgolf')

@section('title', 'Facilities')

@section('content')
<div class="container">
    <div class="container">
        <!-- title -->
        <div class="page-title regular-spacing-section col-12">
            <h1>Facilities</h1>
        </div>
        <!-- /title -->
    </div>

    <!-- intro
    <section class="regular-spacing-section">
        <div class="container">
            <h2 class="section-head text-center">Who We Are</h2>
            <div class="lead col-10 col-sm-8 mx-auto">
                <p>Pastry tootsie roll jelly oat cake icing croissant tiramisu gingerbread donut. Croissant lemon drops halvah marshmallow apple pie tootsie roll. Jujubes bonbon candy candy canes macaroon fruitcake jelly-o danish. Dessert tart caramels cotton candy chocolate bar sugar plum. Pie jelly beans jelly carrot cake topping bonbon gummi bears. Cake carrot cake dessert. Oat cake muffin topping gummi bears sesame snaps toffee cheesecake sweet roll. Tiramisu cake gummi bears cake sweet roll brownie icing marshmallow. Pie chupa chups oat cake muffin tootsie roll liquorice muffin.</p>
            </div>
        </div>
    </section>
    -->

    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Golf-Shop.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Golf Shop</h2>
                    <p>Don’t forget to grab your logo apparel and the latest golf equipment while visiting our well-stocked MountainView Golf Shop. Featuring Callaway, Cobra, Cleveland, Nike, Under Armour, and more. Golf Shop is open 7 AM to 5 PM daily.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Practice-Facilities.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-12 col-lg-7 text-left px-1 px-md-5">
                    <h2>Practice Facilities</h2>
                    <p>MountainView practice facilities feature a large putting green, short game practice area, and a full scale driving range. Our instructional programming covers all parts of the game. Individual, group, and playing lessons are available from our PGA Golf Professionals.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Mesquite-Grill.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Mesquite Grill</h2>
                    <p>Drop by the Grill after a great day on the links! Serving some of the best diner food around for breakfast and lunch. Regular hours are 7 AM to 2 PM Monday – Saturday, and 8 AM to 2 PM on Sundays (serving breakfast all day).</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/The-Bistro.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-12 col-lg-7 text-left px-1 px-md-5">
                    <h2>MountainView Bar & Grill</h2>
                    <p>Ready to grab a relaxing drink at the bar? Starving for a hearty dinner? MVBG is just around the corner! Join us every Tuesday through Saturday. Happy Hour is 3 PM – 6 PM. Dinner is served 4:30 PM – 8 PM.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="spacer"></div>
</div>
@endsection