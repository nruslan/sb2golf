@extends('layouts.mvgolf')

@section('title', 'SaddleBrooke')

@section('slider')
    <div id="carouselExampleIndicators" class="carousel slide lazy" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active text-center">
                <div class="carousel-caption text-center">
                    {{--<div class="display-3">
                        <strong>Lorem ipsum dolor</strong>
                    </div>
                    <p class="display-4">Save Up To 40% Off</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/mv-slider-1-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-1-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-1-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/mv-slider-1-lowres.jpg") }}" class="img-fluid" alt="First slide">
                </picture>
            </div>
            <div class="carousel-item text-center">
                <div class="carousel-caption text-center">
                    {{--<div>
                        <strong>Lorem ipsum dolor</strong>
                    </div>
                    <p>Premium brands</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/mv-slider-2-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-2-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-2-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/mv-slider-2-lowres.jpg") }}" class="img-fluid" alt="Second slide">
                </picture>
            </div>
            <div class="carousel-item text-center">
                <div class="carousel-caption text-center">
                    {{--<div>
                        <strong>Candy canes chocolate bar chocolate </strong>
                    </div>
                    <p>Icing tiramisu icing cake</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/mv-slider-3-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-3-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/mv-slider-3-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/mv-slider-3-lowres.jpg") }}" class="img-fluid" alt="Third slide">
                </picture>
            </div>
        </div>
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endsection

@section('content')
<!-- services -->
{{-- <section class="services py-2 regular-spacing-section bg-dark">
    <div class="container">
        <div class="row d-flex justify-content-around itext">
            <!-- service 1 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_tel"></use>
                    </svg>
                </div>
                <div>
                    <strong>Call Us {{ $phone }}</strong>
                </div>
            </div>
            <!-- /service  -->
            <!-- service 2 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_clock"></use>
                    </svg>
                </div>
                <div>
                    <strong>Hours 7AM - 5PM</strong>
                </div>
            </div>
            <!-- /service  -->
        </div>
    </div>
</section> --}}
<!-- /services -->
<!-- Welcome Section -->
<section class="top-cat regular-spacing-section">
    <div class="container">
        <h2 class="section-head text-center">Welcome to MountainView Golf Club</h2>
        <div class="row no-gutters">
            <!-- cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/tee-time.jpg") }}" alt="Clothing"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('tee.times') }}" target="_blank" onClick="ga('send', 'event', 'Outbound Links', 'Clicks', 'Tee Times Home Page')"><small>Book</small><strong>Your Tee Time</strong></a></h3>
                    </div>
                </div>
            </div>
            <!-- /cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/annual.jpg") }}" alt="Accesories"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('rates.annuals') }}"><small>Annual</small><strong>Membership</strong></a></h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("storage/images/group-events.jpg") }}" alt="Card image"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('outings') }}"><small>Group</small><strong>Events</strong></a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Welcome Section -->

<!-- NEWS Vue Component -->
<news-component></news-component>
<!-- NEWS Vue Component  -->


<!-- EVENTS Vue Component  -->
<events-component></events-component>
<!-- /EVENTS Vue Component  -->

<!-- TEXT CONTENT -->
<section class="regular-spacing-section bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="section-head text-center" id="prod-desc">Terrific Views, Terrific Golf</h2>
            </div>
            <div class="col-md-9 mx-auto lead text-center">
                <p>Set in the foothills of the Catalina Mountains, MountainView Golf Club in Tucson, Arizona is an 18-hole golf course that lives up to its name with magnificent views of the surrounding mountains. Designed by Gary Panks, the course weaves through a hilly, natural desert landscape to create a fascinating layout. The wide-open design often makes for windy conditions, but Panks provides golfers with generous landing areas in the fairways. Mountainview Golf Club is a great track for low and high handicappers alike to enjoy a terrific day on the course.</p>
                <br>
            </div>
        </div>
    </div>
</section>
<!-- /TEXT CONTENT -->

<div class="regular-spacing-section banners">
    <div class="container">
            <br>
        <div class="row">
            <div class="col-sm-6 text-center">
                <div class="banner full-image">
                    <div class="banner-caption">
                        <h4>Facilities <small>Golf, Shop and Dine</small></h4>
                        <a href="{{ route('facilities') }}">Discover MountainView</a>
                    </div>
                    <div class="banner-img">
                        <img src="{{ asset("images/home-page-facilities-image.jpg") }}" class="lazy" alt="Card image">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-center">
                @include('partials.mtview-overview-table')
            </div>
        </div>
    </div>
</div>

{{--<section class="regular-spacing-section feat-brands">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4 col-md-3 col-lg-2 text-center">
                <img data-src="{{ asset("images/assets/brand-1.png") }}" class="img-fluid lazy" alt="brand name">
            </div>
            <div class="col-4 col-md-3 col-lg-2 text-center">
                <img data-src="{{ asset("images/assets/brand-2.png") }}" class="img-fluid lazy" alt="brand name">
            </div>
            <div class="col-4 col-md-3 col-lg-2 text-center">
                <img data-src="{{ asset("images/assets/brand-3.png") }}" class="img-fluid lazy" alt="brand name">
            </div>
            <div class="col-4 col-md-3 col-lg-2 text-center">
                <img data-src="{{ asset("images/assets/brand-1.png") }}" class="img-fluid lazy" alt="brand name">
            </div>
        </div>
    </div>
</section>--}}
@endsection