@extends('layouts.preservegolf')

@section('title', 'Facilities')

@section('content')
<div class="container">
    <div class="container">
        <!-- title -->
        <div class="page-title regular-spacing-section col-12">
            <h1>Facilities</h1>
        </div>
        <!-- /title -->
    </div>

    <!-- intro
    <section class="regular-spacing-section">
        <div class="container">
            <h2 class="section-head text-center">Who We Are</h2>
            <div class="lead col-10 col-sm-8 mx-auto">
                <p>Pastry tootsie roll jelly oat cake icing croissant tiramisu gingerbread donut. Croissant lemon drops halvah marshmallow apple pie tootsie roll. Jujubes bonbon candy candy canes macaroon fruitcake jelly-o danish. Dessert tart caramels cotton candy chocolate bar sugar plum. Pie jelly beans jelly carrot cake topping bonbon gummi bears. Cake carrot cake dessert. Oat cake muffin topping gummi bears sesame snaps toffee cheesecake sweet roll. Tiramisu cake gummi bears cake sweet roll brownie icing marshmallow. Pie chupa chups oat cake muffin tootsie roll liquorice muffin.</p>
            </div>
        </div>
    </section>
    -->

    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Preserve-Golf-Shop.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Golf Shop</h2>
                    <p>Grab your logo apparel and the latest golf equipment while visiting our well-stocked Preserve Golf Shop. Featuring the best from Callaway, Cobra, Cleveland, Nike, Under Armour, and more. Golf Shop is open 7 AM to 5 PM daily.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Practice-Facilities.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-12 col-lg-7 text-left px-1 px-md-5">
                    <h2>Practice Facilities</h2>
                    <p>Preserve practice facilities feature a large putting green, short game practice area, and a practice tee. Our instructional programming covers all parts of the game. Individual, group, and playing lessons are available from our PGA Golf Professionals.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="regular-spacing-section">
        <div class="container">
            <div class="row flex-row-reverse align-items-center">
                <div class="col-lg-5 align-self-center text-center mb-3"><img src="{{ asset('images/Preserve-Restaurant.jpg') }}" alt="" class="img-fluid"></div>
                <div class="lead col-lg-7 text-left px-1 px-md-5">
                    <h2>Preserve Restaurant & Bar</h2>
                    <p>Experience a taste of excellence at The Preserve after a challenging day on the links! A gem among restaurants with dining as spectacular as the views, The Preserve boasts a distinctive menu of Modern American Cuisine. Regular hours are 11 AM to 8 PM, Wednesday – Sunday. Closed Monday and Tuesday.</p>
                </div>
            </div>
        </div>
    </section>
    <div class="spacer"></div>
</div>
@endsection