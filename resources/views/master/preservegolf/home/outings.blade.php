@extends('layouts.preservegolf')

@section('title', 'Group Events')

@section('content')
    <div class="container">
        <div class="container">
            <!-- title -->
            <div class="page-title regular-spacing-section col-12">
                <h1>Group Outings</h1>
            </div>
            <!-- /title -->
        </div>

        <!-- intro -->
        <section class="regular-spacing-section">
            <div class="container">
                <h2 class="section-head text-center">Let Us Exceed Your Golfing Needs</h2>
                <div class="lead col-12 col-sm-12 mx-auto">
                    <p class="text-justify">From small groups and corporate outings to full-field tournaments, MountainView Golf Club is the perfect setting for every type of golf event. We offer flexible packages, including golf only, golf & dining, and golf, dining & meeting / conference venue. Dazzle your guests with fun and challenging play, spectacular mountain views, remarkable dining experiences, and elegant event spaces customized to your needs. Whether you are simply looking to book a foursome with your friends, planning a business get-together, or organizing your corporate event of the year, our professional team is ready to make it a day to remember.</p>
                </div>
            </div>
        </section>
        <!-- /intro -->

        <section class="regular-spacing-section">
            <div class="container">

                <h2 class="section-head text-center">Dining</h2>
                <div class="lead col-10 col-sm-8 mx-auto">
                    <p class="text-center">Our culinary team is always looking to deliver remarkable dining experiences at reasonable rates. Indoor and Outdoor dining spaces are available.</p>
                </div>

                <div class="text-block">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/Mesquite-64-64.jpg') }}" alt="Mesquite Grill Restaurant picture">
                                <div class="media-body">
                                    <h5>Mesquite Grill</h5>
                                    <ul>
                                        <li>Splendid Views of the Golf Course </li>
                                        <li>Best Diner Food Around!</li>
                                        <li>Newly Renovated, Modern & Airy</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/Bistro-64-64.jpg') }}" alt="MountainView Bistro Restaurant picture">
                                <div class="media-body">
                                    <h5>MountainView Bistro</h5>
                                    <ul>
                                        <li>Panoramic Views & Deer Watching</li>
                                        <li>Modern Twist on all-American Favorites</li>
                                        <li>Modern Southwest, Warm & Open</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/Preserve-64-64.jpg') }}" alt="Preserve Bar and Grill picture">
                                <div class="media-body">
                                    <h5>The Preserve Bar & Grill</h5>
                                    <ul>
                                        <li>Impressive Views of Santa Catalinas</li>
                                        <li>Modern American Cuisine</li>
                                        <li>Refined Elegance & Southwest Charm</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="regular-spacing-section">
            <div class="container">

                <h2 class="section-head text-center">Event Venues</h2>
                <div class="lead col-10 col-sm-8 mx-auto">
                    <p class="text-center">Perfect for a meeting, luncheon, or a seminar, our private event spaces can be customized to your needs. Catering Menu is available.</p>
                </div>

                <div class="text-block">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/Meeting-64-64.jpg') }}" alt="Meeting Room picture">
                                <div class="media-body">
                                    <h5>Meeting Rooms</h5>
                                    <ul>
                                        <li>Seating for up to 36</li>
                                        <li>Projectors and Screens</li>
                                        <li>Complimentary Water Station</li>
                                        <li>Audio System and Wi-Fi</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/Ballroom-64-64.jpg') }}" alt="Ballroom picture">
                                <div class="media-body">
                                    <h5>Ballroom</h5>
                                    <ul>
                                        <li>Seating for up to 325</li>
                                        <li>HD Projector &amp; Large Screen</li>
                                        <li>Small Stage and Dance Floor</li>
                                        <li>PA System and Wi-Fi</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="media">
                                <img src="{{ asset('images/DVPAC-64-64.jpg') }}" alt="DesertView Theater picture">
                                <div class="media-body">
                                    <h5>Theater</h5>
                                    <ul>
                                        <li>Seating for up to 478</li>
                                        <li>HD Projector &amp; Movie Screen</li>
                                        <li>Handicap Accessible / T-Coil</li>
                                        <li>PA System &amp; Wi-fi</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Inquiry Form -->
        <outings-inquiry-component></outings-inquiry-component>
        <!-- /Inquiry Form -->
        <div class="spacer"></div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection