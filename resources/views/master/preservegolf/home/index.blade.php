@extends('layouts.preservegolf')

@section('title', 'SaddleBrooke')

@section('slider')
    <div id="carouselExampleIndicators" class="carousel slide lazy" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active text-center">
                <div class="carousel-caption text-center">
                    {{--<div class="display-3">
                        <strong>Lorem ipsum dolor</strong>
                    </div>
                    <p class="display-4">Save Up To 40% Off</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/pr-slider-1-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-1-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-1-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/pr-slider-1-lowres.jpg") }}" class="img-fluid" alt="First slide">
                </picture>
            </div>
            <div class="carousel-item text-center">
                <div class="carousel-caption text-center">
                    {{--<div>
                        <strong>Lorem ipsum dolor</strong>
                    </div>
                    <p>Premium brands</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/pr-slider-2-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-2-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-2-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/pr-slider-2-lowres.jpg") }}" class="img-fluid" alt="Second slide">
                </picture>
            </div>
            <div class="carousel-item text-center">
                <div class="carousel-caption text-center">
                    {{--<div>
                        <strong>Candy canes chocolate bar chocolate </strong>
                    </div>
                    <p>Icing tiramisu icing cake</p>--}}
                </div>
                <picture>
                    <source srcset="{{ asset("storage/images/slider/pr-slider-3-sm.jpg") }}" media="(max-width: 768px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-3-md.jpg") }}" media="(max-width: 1124px)">
                    <source srcset="{{ asset("storage/images/slider/pr-slider-3-lg.jpg") }}">
                    <img src="{{ asset("storage/images/slider/pr-slider-3-lowres.jpg") }}" class="img-fluid" alt="Third slide">
                </picture>
            </div>
        </div>
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endsection

@section('content')
<!-- services -->
{{-- <section class="services py-2 regular-spacing-section bg-dark">
    <div class="container">
        <div class="row d-flex justify-content-around itext">
            <!-- service 1 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_tel"></use>
                    </svg>
                </div>
                <div>
                    <strong>Call Us {{ $phone }}</strong>
                </div>
            </div>
            <!-- /service  -->
            <!-- service 2 -->
            <div class="d-flex service-item">
                <div class="d-none d-sm-block">
                    <svg class="icon">
                        <use xlink:href="images/assets/svg/symbols.svg#icon_clock"></use>
                    </svg>
                </div>
                <div>
                    <strong>Hours 7AM - 5PM</strong>
                </div>
            </div>
            <!-- /service  -->
        </div>
    </div>
</section> --}}
<!-- /services -->
<!-- Welcome Section -->
<section class="top-cat regular-spacing-section">
    <div class="container">
        <h2 class="section-head text-center">Welcome to {{ $club_name }}</h2>
        <div class="row no-gutters">
            <!-- cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/Preserve-Tee-Time.jpg") }}" alt="Clothing"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('preserve.teetimes') }}" target="_blank"><small>Book</small><strong>Your Tee Time</strong></a></h3>
                    </div>
                </div>
            </div>
            <!-- /cat 1 -->
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/annual.jpg") }}" alt="Accesories"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('preserve.rates.annuals') }}"><small>Annual</small><strong>Membership</strong></a></h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 text-center">
                <div class="card text-white">
                    <a href="#"><img class="card-img clip img-fluid lazy" src="{{ asset("images/assets/categorie-banner-3.jpg") }}" alt="Card image"></a>
                    <div class="card-img-overlay cat-overlay">
                        <h3 class="card-title text-center"><a href="{{ route('preserve.outings') }}"><small>Group</small><strong>Events</strong></a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Welcome Section -->

<!-- NEWS Vue Component -->
<news-component></news-component>
<!-- NEWS Vue Component  -->


<!-- EVENTS Vue Component  -->
<events-component></events-component>
<!-- /EVENTS Vue Component  -->

<!-- TEXT CONTENT -->
<section class="regular-spacing-section bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="section-head text-center" id="prod-desc">Golf On Top of the World</h2>
            </div>
            <div class="col-md-9 mx-auto lead text-center">
                <p>Winding through the beautiful canyons and foothills of the Santa Catalina Mountains, The Preserve Golf Club 18-hole Championship Course gives you the feeling that you’re literally on top of the world. The layout features rolling fairways and undulating greens, presenting an enjoyable challenge to novices as well as avid golfers. Boasting excellent views and dramatic elevation changes, The Preserve is a terrific choice for your regular rounds of golf, or as a new destination for you and friends when visiting Tucson.</p>
                <br>
            </div>
        </div>
    </div>
</section>
<!-- /TEXT CONTENT -->

<div class="regular-spacing-section banners">
    <div class="container">
        <br>
        <div class="row">
            <div class="col-sm-6 text-center">
                <div class="banner full-image">
                    <div class="banner-caption">
                        <h4>Facilities <small>Golf, Shop and Dine</small></h4>
                        <a href="{{ route('preserve.facilities') }}">Discover The Preserve</a>
                    </div>
                    <div class="banner-img">
                        <img src="{{ asset("images/home-page-preserve-facilities-image.jpg") }}" class="lazy" alt="Card image">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-center">
                @include('partials.preserve-overview-table')
            </div>
        </div>
    </div>
</div>
@endsection