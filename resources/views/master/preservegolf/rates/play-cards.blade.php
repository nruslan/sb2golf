@extends('layouts.preservegolf')

@section('title', 'Play Cards')

@section('content')
    <div class="container">
        <div class="container">
            <!-- title -->
            <div class="page-title regular-spacing-section col-12">
                <h1>Play Cards</h1>
            </div>
            <!-- /title -->
        </div>
        @include('partials.play-cards')
        <div class="spacer"></div>
    </div>
@endsection