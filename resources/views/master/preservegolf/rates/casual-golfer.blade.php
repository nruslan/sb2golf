@extends('layouts.preservegolf')

@section('title', 'Casual Golfer')

@section('content')
    <div class="container">
        <div class="container">
            <!-- title -->
            <div class="page-title regular-spacing-section col-12">
                <h1>Casual Golfer Program</h1>
            </div>
            <!-- /title -->
        </div>
        @include('partials.casual-golfer')
        <div class="spacer"></div>
    </div>
@endsection