@extends('layouts.admin')

@section('title', 'All Events')

@section('content')
    <admin-events-component></admin-events-component>
@endsection