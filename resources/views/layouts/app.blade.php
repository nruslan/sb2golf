<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W2CQJ3D');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Ruslan Niyazimbetov">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{ $club_name }}</title>
    <!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

    <script>
        //load google fonts async
        WebFontConfig = { google: { families: [ 'Roboto+Condensed:300,400,700' ] } };
        (function() {
            let wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            let s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
</head>
<body class="bg-light">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2CQJ3D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
    <!-- header -->
    <header  id="main-hd">
        <div class="navigation">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="logo" href="/"><img src="{{ asset('images/sb2golf.svg') }}" alt="SaddleBrooke 2 Golf Logotype"></a>

                    <div class="collapse navbar-collapse" id="main-nav">
                        <button type="button" class="close d-lg-none" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">Tee Times</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="{{ route('tee.times.mountainview') }}" target="_blank">MountainView Golf Course</a>
                                    <a class="dropdown-item" href="{{ route('tee.times.preserve') }}" target="_blank">Preserve Golf Course</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">Rates</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="{{ route('sb2rates.annuals') }}">Annuals</a>
                                    <a class="dropdown-item" href="{{ route('sb2rates.play-cards') }}">Play Cards</a>
                                    <a class="dropdown-item" href="{{ route('sb2rates.casual-golfer') }}">Casual Golfer</a>
                                </div>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('sb2facilities') }}">Facilities</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('group.outings') }}">Outings</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('sb2contact.index') }}">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- nav -->
            </div>
        </div>
    </header>
    <!-- /hd -->

    <!-- SLIDER -->
    @yield('slider')
    <!-- /END SLIDER -->

    <!-- MAIN CONTENT -->
    <main>@yield('content')</main>
    <!-- /END MAIN CONTENT -->

    <!-- FOOTER -->
    <footer id="ft">
        <div class="ft-top py-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 mt-2">

                        <div class="lead-adquisition mr-3">
                            <h3 class="widget-title-l1">Sign Up for Newsletters</h3>
                            <p>Get E-mail updates about SaddleBrooke Golf news and upcoming events!</p>
                            <subscribe-component></subscribe-component>
                        </div>
                    </div>
                    <div class="col-sm-6 mt-2">

                        <h3 class="text-center widget-title-l1">Fun Courses, Great Views</h3>
                        <div class="testimonial-area media">
                            <img data-src="{{ asset('images/reviews.svg') }}" alt="testimonial" class="mx-3 lazy">
                            <div class="media-body">
                                <blockquote class="testimonials-text">
                                    <p><em>Experience MountainView and The Preserve for yourself to find out why golfers are saying: "This course is fantastic. If you get a chance to play there, don’t miss it." "Always a friendly and helpful atmosphere." "Loved this course. For Tucson area it was a surprise." "Nice track. Deer everywhere!"</em></p>
                                </blockquote>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="ft-widgets-container py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="row align-items-center">
                            {{--<div class="col-5">
                                <div class="logo mr-2">
                                    <img src="{{ asset('images/assets/icon_mvgc-logo.svg') }}" alt="MVGC Logotype">
                                </div>
                            </div>--}}
                            <div class="col-7">
                                <h4 class="widget-title-l2">{{ $club_name }}</h4>
                                <ul class="list-unstyled address-list">
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_location"></use></svg> 66567 East Catalina Hills Drive<br>SaddleBrooke<br>Tucson AZ</li>
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_email"></use></svg> {{ eMail::mutate($email) }}</li>
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_tel"></use></svg> {{ $phone }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-6 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Main Menu</h4>
                                <ul class="list-unstyled">
                                    <li><a href="{{ route('tee.times.mountainview.footer') }}" target="_blank">MountainView Tee Times</a></li>
                                    <li><a href="{{ route('tee.times.preserve.footer') }}" target="_blank">Preserve Tee Times</a></li>
                                    <li><a href="{{ route('sb2facilities') }}">Facilities</a></li>
                                    <li><a href="{{ route('sb2rates.annuals') }}">Annuals</a></li>
                                    <li><a href="{{ route('group.outings') }}">Group Outings</a></li>
                                </ul>
                            </div>
                            <div class="col-6 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Related</h4>
                                <ul class="list-unstyled">
                                    <li><a href="https://mountainviewgolf.club" target="_blank">MountainView Golf Club</a></li>
                                    <li><a href="https://preservegolf.club" target="_blank">The Preserve Golf Club</a></li>
                                    <li><a href="http://sbhoa2.org" target="_blank">SaddleBrooke TWO</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Follow us</h4>
                                <ul class="list-unstyled social-links list-inline">
                                    <li class="list-inline-item"><a href="https://www.facebook.com/TPGolfClub/" target="_blank"><svg class="icon big"><use xlink:href="images/assets/svg/symbols.svg#icon_facebook"></use></svg></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg-light py-2">
            <div class="container">
                <div class="row copy-info">
                    <div class="col-12 text-center">
                        <p>SaddleBrooke HOA 2, Inc &copy; 2019 - {{ date('Y') }} {{ $club_name }}</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Scripts -->
<script src="{{ mix('/js/app.js') }}"></script>
<script>
    window.onload = function() {
        new LazyLoad({
            elements_selector: ".lazy"
            // ... more custom settings?
        });
    }
</script>
@yield('script')
<script>
    $('[data-toggle="tooltip"]').tooltip()
</script>
</body>
</html>
