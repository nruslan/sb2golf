<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N5B226Z');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Ruslan Niyazimbetov">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | MountainView Golf Club</title>
    <!-- Styles -->
    <link href="{{ mix('/css/mvgolf.css') }}" rel="stylesheet">

    <!-- Generic -->
    <link rel="icon" href="{{ asset('favicons/favicon-32.png') }}" sizes="32x32">
    <link rel="icon" href="{{ asset('favicons/favicon-57.png') }}" sizes="57x57">
    <link rel="icon" href="{{ asset('favicons/favicon-76.png') }}" sizes="76x76">
    <link rel="icon" href="{{ asset('favicons/favicon-96.png') }}" sizes="96x96">
    <link rel="icon" href="{{ asset('favicons/favicon-128.png') }}" sizes="128x128">
    <link rel="icon" href="{{ asset('favicons/favicon-192.png') }}" sizes="192x192">
    <link rel="icon" href="{{ asset('favicons/favicon-228.png') }}" sizes="228x228">

    <!-- Android -->
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('favicons/favicon-196.png') }}">

    <!-- Web App Manifest -->
    <link rel="manifest" href="{{ asset('favicons/site.webmanifest') }}">

    <!-- Windows 8 IE 10 -->
    <meta name="msapplication-TileColor" content="#94AE31">
    <meta name="msapplication-TileImage" content="{{ asset('favicons/favicon-144.png') }}">

    <!-- Windows 8.1 + IE11 and above -->
    <meta name="msapplication-config" content="{{ asset('favicons/browserconfig.xml') }}">

    <!-- iOS -->
    <link rel="apple-touch-icon" href="{{ asset('favicons/favicon-120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ asset('favicons/favicon-152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ asset('favicons/favicon-180.png') }}" sizes="180x180">

    <script>
        //load google fonts async
        WebFontConfig = { google: { families: [ 'Roboto+Condensed:300,400,700' ] } };
        (function() {
            let wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            let s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '547487802672520');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=547487802672520&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body class="bg-light">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5B226Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
    <!-- header -->
    <header  id="main-hd">
        <div class="navigation">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="logo" href="/"><img src="{{ asset('images/assets/icon_mvgc-logo.svg') }}" alt="MVGC Logotype"></a>

                    <div class="collapse navbar-collapse" id="main-nav">
                        <button type="button" class="close d-lg-none" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="{{ route('index') }}">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('tee.times') }}" target="_blank" onClick="ga('send', 'event', 'Outbound Links', 'Clicks', 'Tee Times Top Nav')">Tee Times</a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">Rates</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="{{ route('rates.annuals') }}">Annuals</a>
                                    <a class="dropdown-item" href="{{ route('rates.play-cards') }}">Play Cards</a>
                                    <a class="dropdown-item" href="{{ route('rates.casual-golfer') }}">Casual Golfer</a>
                                </div>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('facilities') }}">Facilities</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('outings') }}">Outings</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ route('contact.index') }}">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- nav -->
            </div>
        </div>
    </header>
    <!-- /hd -->

    <!-- SLIDER -->
    @yield('slider')
    <!-- /END SLIDER -->

    <!-- MAIN CONTENT -->
    <main>
        @yield('content')
    </main>
    <!-- /END MAIN CONTENT -->

    <!-- FOOTER -->
    <footer id="ft">
        <div class="ft-top py-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 mt-2">

                        <div class="lead-adquisition mr-3">
                            <h3 class="widget-title-l1">Sign Up for Newsletters</h3>
                            <p>Get E-mail updates about MV Golf news and upcoming events!</p>
                            <subscribe-component></subscribe-component>
                        </div>
                    </div>
                    <div class="col-sm-6 mt-2">

                        <h3 class="text-center widget-title-l1">Worth the Drive</h3>
                        <div class="testimonial-area media">
                            <img data-src="{{ asset('images/reviews.svg') }}" alt="testimonial" class="mx-3 lazy">
                            <div class="media-body">
                                <blockquote class="testimonials-text">
                                    <p><em>Experience MountainView Golf Club for yourself and find out why golfers are saying: "This is a wonderful course to play. Great layout." "Greens are terrific." "Fun course. Great views." "Always a friendly and helpful atmosphere." "Nice track. Deer everywhere!" "The course is fantastic."</em></p>
                                </blockquote>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="ft-widgets-container py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="row align-items-center">
                            {{--<div class="col-5">
                                <div class="logo mr-2">
                                    <img src="{{ asset('images/assets/icon_mvgc-logo.svg') }}" alt="MVGC Logotype">
                                </div>
                            </div>--}}
                            <div class="col-7">
                                <h4 class="widget-title-l2">{{ $club_name }}</h4>
                                <ul class="list-unstyled address-list">
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_location"></use></svg> 38691 S MountainView Blvd<br>SaddleBrooke<br>Tucson AZ</li>
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_email"></use></svg> {{ eMail::mutate($email) }}</li>
                                    <li><svg class="icon small"><use xlink:href="images/assets/svg/symbols.svg#icon_tel"></use></svg> {{ $phone }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-6 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Main Menu</h4>
                                <ul class="list-unstyled">
                                    <li><a href="{{ route('tee.times') }}" target="_blank">Tee Times</a></li>
                                    <li><a href="{{ route('facilities') }}">Facilities</a></li>
                                    <li><a href="{{ route('rates.annuals') }}">Annuals</a></li>
                                    <li><a href="{{ route('outings') }}">Group Events</a></li>
                                </ul>
                            </div>
                            <div class="col-6 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Related</h4>
                                <ul class="list-unstyled">
                                    <li><a href="https://preservegolf.club" target="_blank">The Preserve Golf Club</a></li>
                                    <li><a href="http://sbhoa2.org" target="_blank">SaddleBrooke TWO</a></li>
                                    <li><a href="https://dvpac.net" target="_blank">DesertView Theater</a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 ft-widget">
                                <h4 class="widget-title-l2">Follow us</h4>
                                <ul class="list-unstyled social-links list-inline">
                                    <li class="list-inline-item"><a href="https://www.facebook.com/SBMVGolfClub/" target="_blank"><svg class="icon big"><use xlink:href="images/assets/svg/symbols.svg#icon_facebook"></use></svg></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg-light py-2">
            <div class="container">
                <div class="row copy-info">
                    <div class="col-12 text-center">
                        <p>SaddleBrooke HOA 2, Inc &copy; 2019 - {{ date('Y') }} {{ $club_name }}</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Scripts -->
<script src="{{ mix('/js/mvgolf.js') }}"></script>
<script>
    window.onload = function() {
        new LazyLoad({
            elements_selector: ".lazy"
            // ... more custom settings?
        });
    }
</script>
@yield('script')
</body>
</html>
