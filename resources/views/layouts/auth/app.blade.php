<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="SaddleBrooke Golf">
    <meta name="author" content="Ruslan Niyazimbetov">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body @yield('class')>

<div id="app">
    @yield('content')
    <p class="text-center">Developed and Maintained by <a href="https://nruslan.com" data-toggle="tooltip" data-placement="top" title="Ruslan Niyazimbetov" target="_blank">Ruslan N</a> &copy; 2018 - {{ date('Y') }}</p>
</div>

<!-- Scripts -->
<script src="{{ asset('js/admin.js') }}"></script>
<script>
    $('[data-toggle="tooltip"]').tooltip();
</script>
@yield('script')
</body>
</html>