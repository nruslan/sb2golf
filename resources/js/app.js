
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
window.LazyLoad = require('vanilla-lazyload').default;

// SweetAlert2
import Swal from 'sweetalert2';
window.Swal = Swal;

const Toast = Swal.mixin({
    //toast: true,
    position: 'center',
    type: 'success',
    showConfirmButton: false,
    timer: 3000
});
window.Toast = Toast;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('subscribe-component', require('./components/sb2golf/SubscribeFormComponent.vue').default);
Vue.component('outings-inquiry-component', require('./components/sb2golf/OutingsInquiryComponent.vue').default);
Vue.component('contact-us-component', require('./components/sb2golf/ContactUsComponent').default);
Vue.component('news-component', require('./components/sb2golf/NewsComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
