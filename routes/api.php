<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::domain(config('app.domain_1'))->group(function () {
    Route::namespace('Sb2Golf')->group(function () {
        Route::get('news', 'NewsController@index');

        Route::post('subscribe', 'ContactController@subscribe');
        Route::post('outings-inquiry', 'ContactController@outingsInquiry');
        Route::post('contact-us', 'ContactController@contactForm');
    });
});

Route::domain(config('app.domain_2'))->group(function () {
    Route::namespace('MvGolf')->group(function () {
        Route::get('events', 'EventController@index');
        Route::get('news', 'NewsController@index');

        Route::post('subscribe', 'ContactController@subscribe');
        Route::post('outings-inquiry', 'ContactController@outingsInquiry');
        Route::post('contact-us', 'ContactController@contactForm');
    });
});

Route::domain(config('app.domain_3'))
    ->namespace('PreserveGolf')
    ->group(function () {
        Route::get('events', 'EventController@index');
        Route::get('news', 'NewsController@index');

        Route::post('subscribe', 'ContactController@subscribe');
        Route::post('outings-inquiry', 'ContactController@outingsInquiry');
        Route::post('contact-us', 'ContactController@contactForm');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('admin')->group(function () {
    Route::apiResources([
        'events' => 'Admin\API\EventsController'
    ]);
});

