<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Domain #1 SaddleBrooke Golf
 ---------------------------------------------------------------------------------------------------------------------*/
Route::domain(config('app.domain_1'))->group(function () {
    //SaddleBrooke Golf
    Route::namespace('Sb2Golf')->group(function () {
        Route::get('/', 'HomePageController@index')->name('home');
        //Top navigation
        Route::get('/tee-times/mountainview', 'TeeTimesController@mountainview')->name('tee.times.mountainview');
        Route::get('/tee-times/preserve', 'TeeTimesController@preserve')->name('tee.times.preserve');
        //Footer
        Route::get('/tee-times-mountainview', 'TeeTimesController@mountainview')->name('tee.times.mountainview.footer');
        Route::get('/tee-times-preserve', 'TeeTimesController@preserve')->name('tee.times.preserve.footer');

        Route::get('/golf/mountainview', 'HomePageController@mountainview')->name('golf.mountainview');
        Route::get('/golf/preserve', 'HomePageController@preserve')->name('golf.preserve');
        Route::get('/outings', 'HomePageController@outings')->name('group.outings');
        Route::get('/facilities', 'HomePageController@facilities')->name('sb2facilities');
        Route::get('/rates', 'RatesController@index')->name('sb2rates');
        Route::get('/annuals', 'RatesController@annuals')->name('sb2rates.annuals');
        Route::get('/play-cards', 'RatesController@playCards')->name('sb2rates.play-cards');
        Route::get('/casual-golfer', 'RatesController@casualGolfer')->name('sb2rates.casual-golfer');
        Route::get('/contact', 'ContactController@index')->name('sb2contact.index');
        // Route::get('/', function (){
        //     return view("master.sb2golf.under-construction.index");
        // });
        // Route::get('/', function (){
        //     return redirect('http://sbhoa2.org/golf');
        // });
    });


    // Admin Routs
    Auth::routes(['register' => false, 'verify' => true]);
    Route::namespace('Admin')
        ->prefix('admin')
        ->name('admin.')
        ->middleware('verified')
        ->group(function () {
            // Controllers Within The "App\Http\Controllers\Admin" Namespace
            Route::get('/', function (){
                return redirect()->route('admin.dashboard');
            });
            Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
            Route::get('/events', 'EventsController')->name('events');
    });
});

/**
 * Domain #2 MountainView Golf Club
 ---------------------------------------------------------------------------------------------------------------------*/
Route::domain(config('app.domain_2'))->group(function () {
    //Redirect to Admin Panel
    Route::get('/admin', function (){
        return redirect(config('app.domain_1').'/admin');
    });
    //MountainView Golf
    Route::namespace('MvGolf')->group(function () {
        Route::get('/', 'HomePageController@index')->name('index');
        Route::get('/tee-times', 'TeeTimesController@index')->name('tee.times');

        Route::get('/rates', 'RatesController@index')->name('rates');
        Route::get('/annuals', 'RatesController@annuals')->name('rates.annuals');
        Route::get('/play-cards', 'RatesController@playCards')->name('rates.play-cards');
        Route::get('/casual-golfer', 'RatesController@casualGolfer')->name('rates.casual-golfer');

        Route::get('/facilities', 'HomePageController@facilities')->name('facilities');
        Route::get('/outings', 'HomePageController@outings')->name('outings');
        Route::get('/contact', 'ContactController@index')->name('contact.index');
    });
});

/**
 * Domain #3 The Preserve Golf Club
 ---------------------------------------------------------------------------------------------------------------------*/
Route::domain(config('app.domain_3'))->group(function () {
    //Redirect to Admin Panel
    Route::get('/admin', function (){
        return redirect(config('app.domain_1').'/admin');
    });
    //The Preserve Golf
    Route::namespace('PreserveGolf')
        ->name('preserve.')
        ->group(function () {
            Route::get('/', 'HomePageController@index')->name('index');
            Route::get('/tee-times', 'TeeTimesController@index')->name('tee.times');
            Route::get('/teetimes', 'TeeTimesController@index')->name('teetimes');

            Route::get('/rates', 'RatesController@index')->name('rates');
            Route::get('/annuals', 'RatesController@annuals')->name('rates.annuals');
            Route::get('/play-cards', 'RatesController@playCards')->name('rates.play-cards');
            Route::get('/casual-golfer', 'RatesController@casualGolfer')->name('rates.casual-golfer');

            Route::get('/facilities', 'HomePageController@facilities')->name('facilities');
            Route::get('/outings', 'HomePageController@outings')->name('outings');
            Route::get('/contact', 'ContactController@index')->name('contact.index');
        });
});