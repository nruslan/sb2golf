<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'picture_id', 'document_id', 'expired_at', 'title', 'subtitle', 'description'
    ];

    public function picture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function document()
    {
        return $this->belongsTo('App\Document', 'document_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getNewsPictureAttribute()
    {
        return $this->picture_id
            ? $this->picture->pic_path
            : 'news/images/blank.jpg';
    }

    public function getShortDescriptionAttribute()
    {
        $start = 0;
        $strLength = 513;

        //Remove any titles <h1> - <h6>
        $no_title_txt = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $this->description);

        // Remove HTML tags from the content
        $txt = strip_tags($no_title_txt);

        return strlen($txt) > $strLength
            ? substr($txt, $start, strrpos($txt, " ", -(strlen($txt) - $strLength)))
            : $txt;
    }

    /**
     * Get PDF Document
     * @return string
     */
    public function getPdfDocumentAttribute()
    {
        return $this->document_id
            ? $this->document->doc_path
            : 'default.pdf';
    }
}
