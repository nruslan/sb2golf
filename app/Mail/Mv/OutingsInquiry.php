<?php

namespace App\Mail\Mv;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OutingsInquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $inquiryData;

    /**
     * Create a new message instance.
     * @param $inquiryData
     * @return void
     */
    public function __construct($inquiryData)
    {
        $this->inquiryData = $inquiryData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->inquiryData->email)
            ->subject('MV Golf Club Outings Inquiry')
            ->markdown('emails.mv.outings-inquiry')
            ->with([
                'name' => $this->inquiryData->name,
                'email' => $this->inquiryData->email,
                'phone' => $this->inquiryData->phone,
                'bodyMessage' => $this->inquiryData->event_details
            ]);
    }
}
