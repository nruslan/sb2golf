<?php

namespace App\Mail\Mv;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactFormMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $inputData;
    /**
     * Create a new message instance.
     * @param $inputData
     * @return void
     */
    public function __construct($inputData)
    {
        $this->inputData = $inputData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->inputData->email)
            ->subject('MV Golf Club Contact Form')
            ->markdown('emails.mv.contact-form')
            ->with([
                'name' => $this->inputData->name,
                'email' => $this->inputData->email,
                'bodyMessage' => $this->inputData->input_comments
            ]);
    }
}
