<?php

namespace App\Http\Controllers\PreserveGolf;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $template;
    public $clubname;

    public function __construct()
    {
        $this->clubname = 'The Preserve Golf Club';
        $this->template = 'master.preservegolf';
    }

    public function nowDate()
    {
        return Carbon::now();
    }
}
