<?php

namespace App\Http\Controllers\PreserveGolf;

class TeeTimesController extends BaseController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('https://www.golf18network.com/arizona/tucson/the-preserve/golf/course/tee-times/7351.php?showall=1&display=embeddedWide');
    }
}
