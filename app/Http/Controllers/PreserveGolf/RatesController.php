<?php

namespace App\Http\Controllers\PreserveGolf;

use Illuminate\Http\Request;

class RatesController extends BaseController
{
    protected $template;

    public function __construct()
    {
        parent::__construct();
        $this->template = "$this->template.rates";
    }

    public function index()
    {
        return view("$this->template.index");
    }

    public function annuals()
    {
        return view("$this->template.annuals");
    }

    public function playCards()
    {
        return view("$this->template.play-cards");
    }

    public function casualGolfer()
    {
        return view("$this->template.casual-golfer");
    }
}
