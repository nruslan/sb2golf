<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class EventsController extends BaseController
{
    protected $template;

    public function __construct()
    {
        parent::__construct();
        $this->template = 'admin';
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return view("$this->template.events");
    }
}
