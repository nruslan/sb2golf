<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController extends BaseController
{
    protected $template;

    public function __construct()
    {
        parent::__construct();
        $this->template = 'admin';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view("$this->template.dashboard");
    }
}
