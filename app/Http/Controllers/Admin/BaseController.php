<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->middleware('auth');
        //$this->template = 'master.contact';
    }
}
