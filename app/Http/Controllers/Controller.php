<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const TO_MAIL = ['Matt Hudson' => 'matt.hudson@sbhoa2.org'];
    const CC_MAIL = ['Mike Karpe' => 'mike.karpe@sbhoa2.org'];
    const BCC_MAIL = ['Kathy N' => 'kathy@sbhoa2.org'];
}
