<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomePageController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = 'master.home';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view("$this->template.index");
    }

    public function membership()
    {
        return view("$this->template.membership");
    }

    public function groupEvents()
    {
        return view("$this->template.events");
    }

    public function facilities()
    {
        return view("$this->template.facilities");
    }
}
