<?php

namespace App\Http\Controllers\Sb2Golf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeeTimesController extends Controller
{
    public function mountainview()
    {
        return redirect('https://www.golf18network.com/arizona/tucson/mountain-view/golf/course/tee-times/7352.php?showall=1');
    }

    public function preserve()
    {
        return redirect('https://www.golf18network.com/arizona/tucson/the-preserve/golf/course/tee-times/7351.php?showall=1&display=embeddedWide');
    }
}
