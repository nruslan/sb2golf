<?php

namespace App\Http\Controllers\Sb2Golf;

use App\News;
use App\Http\Resources\News as NewsResource;

class NewsController extends BaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $news = News::with('picture')
            ->where('expired_at', '>', $this->nowDate())
            ->orWhere('expired_at', null)
            ->orderByDesc('created_at')
            ->paginate(2);
        return NewsResource::collection($news);
    }
}
