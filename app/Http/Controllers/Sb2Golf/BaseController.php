<?php

namespace App\Http\Controllers\Sb2Golf;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = 'master.sb2golf';
    }

    public function nowDate()
    {
        return Carbon::now();
    }
}
