<?php

namespace App\Http\Controllers\Sb2Golf;

use App\Mail\Sb2Golf\{ContactFormMessage, OutingsInquiry};
use Ctct\Components\Contacts\Contact;
use Ctct\ConstantContact;
use Ctct\Exceptions\CtctException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends BaseController
{
    const API_KEY = 'f86ns6awb9ngycsyagsq3ts2';
    const ACCESS_TOKEN = 'c5400d2f-aee9-40a5-9735-274eb82867d6';
    const MVG_ID = '2013049132';

    const API_KEY_PRESERVE = '6guh6t7r7cfhssv6gbxeynmw';
    const ACCESS_TOKEN_PRESERVE = 'eb618e20-8681-4678-90bf-11e3d760f16e';
    const PRESERVE_ID = '1869203453';

    const ACTION_BY = 'ACTION_BY_OWNER'; // ACTION_BY_VISITOR



    const TO_MAIL = ['Matt Hudson' => 'matt.hudson@sbhoa2.org'];
    const CC_MAIL = ['Mike Karpe' => 'mike.karpe@sbhoa2.org'];
    const BCC_MAIL = ['Kathy N' => 'kathy@sbhoa2.org'];

    protected $template;

    public function __construct()
    {
        parent::__construct();
        $this->template = "$this->template.contact";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view("$this->template.index");
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $cc = new ConstantContact(self::API_KEY);
        $cc_preserve = new ConstantContact(self::API_KEY_PRESERVE);
        // attempt to fetch lists in the account, catching any exceptions and printing the errors to screen
        try {
            $lists = $cc->listService->getLists(self::ACCESS_TOKEN);
            $lists_preserve = $cc_preserve->listService->getLists(self::ACCESS_TOKEN_PRESERVE);
        } catch (CtctException $ex) {
            foreach ($ex->getErrors() as $error) {
                print_r($error);
            }
            if (!isset($lists) || !isset($lists_preserve)) {
                $lists = null;
                $lists_preserve = null;
            }
        }
        // check if the form was submitted
        if (isset($request->email) && strlen($request->email) > 1) {
            $action = "Getting Contact By Email Address";
            try {
                // check to see if a contact with the email address already exists in the account
                $response = $cc->contactService->getContacts(self::ACCESS_TOKEN, ["email" => $request->email]);
                
                // create a new contact if one does not exist
                if (empty($response->results)) {
                    $action = "Creating Contact";
                    $contact = new Contact();
                    $contact->addEmail($request->email);
                    $contact->addList(self::MVG_ID);
                    $contact->first_name = '';
                    $contact->last_name = '';
                    /*
                     * The third parameter of addContact defaults to false, but if this were set to true it would tell Constant
                     * Contact that this action is being performed by the contact themselves, and gives the ability to
                     * opt contacts back in and trigger Welcome/Change-of-interest emails.
                     *
                     * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                     */
                    $returnContact = $cc->contactService->addContact(self::ACCESS_TOKEN, $contact, ['action_by' => self::ACTION_BY]);
                    // update the existing contact if address already existed
                } else {
                    $action = "Updating Contact";
                    $contact = $response->results[0];
                    if ($contact instanceof Contact) {
                        $contact->addList(self::MVG_ID);
                        $contact->first_name = '';
                        $contact->last_name = '';
                        /*
                         * The third parameter of updateContact defaults to false, but if this were set to true it would tell
                         * Constant Contact that this action is being performed by the contact themselves, and gives the ability to
                         * opt contacts back in and trigger Welcome/Change-of-interest emails.
                         *
                         * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                         */
                        $returnContact = $cc->contactService->updateContact(self::ACCESS_TOKEN, $contact, ['action_by' => self::ACTION_BY]);
                    } else {
                        $e = new CtctException();
                        $e->setErrors(["type", "Contact type not returned"]);
                        throw $e;
                    }
                }

                //PRESERVE Email List
                $response_preserve = $cc_preserve->contactService->getContacts(self::ACCESS_TOKEN_PRESERVE, ["email" => $request->email]);

                // create a new contact if one does not exist
                if (empty($response_preserve->results)) {
                    $action = "Creating Contact";
                    $contact = new Contact();
                    $contact->addEmail($request->email);
                    $contact->addList(self::PRESERVE_ID);
                    $contact->first_name = '';
                    $contact->last_name = '';
                    /*
                    * The third parameter of addContact defaults to false, but if this were set to true it would tell Constant
                    * Contact that this action is being performed by the contact themselves, and gives the ability to
                    * opt contacts back in and trigger Welcome/Change-of-interest emails.
                    *
                    * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                    */
                    $returnContact = $cc_preserve->contactService->addContact(self::ACCESS_TOKEN_PRESERVE, $contact, ['action_by' => self::ACTION_BY]);
                    // update the existing contact if address already existed
                } else {
                    $action = "Updating Contact";
                    $contact = $response_preserve->results[0];
                    if ($contact instanceof Contact) {
                        $contact->addList(self::PRESERVE_ID);
                        $contact->first_name = '';
                        $contact->last_name = '';
                        /*
                        * The third parameter of updateContact defaults to false, but if this were set to true it would tell
                        * Constant Contact that this action is being performed by the contact themselves, and gives the ability to
                        * opt contacts back in and trigger Welcome/Change-of-interest emails.
                        *
                        * See: http://developer.constantcontact.com/docs/contacts-api/contacts-index.html#opt_in
                        */
                        $returnContact = $cc_preserve->contactService->updateContact(self::ACCESS_TOKEN_PRESERVE, $contact, ['action_by' => self::ACTION_BY]);
                    } else {
                        $e = new CtctException();
                        $e->setErrors(["type", "Contact type not returned"]);
                        throw $e;
                    }
                }

                return $action;
                // catch any exceptions thrown during the process and print the errors to screen
            } catch (CtctException $ex) {
                echo '<span class="label label-important">Error ' . $action . '</span>';
                echo '<div class="container alert-error"><pre class="failure-pre">';
                print_r($ex->getErrors());
                echo '</pre></div>';
                die();
            }
        }
    }

    public function outingsInquiry(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:191',
            'email' => 'required|email',
            'event_details' => 'required|max:500'
        ]);

        Mail::to(self::TO_MAIL)
            ->cc(self::CC_MAIL)
            ->bcc(self::BCC_MAIL)
            ->send(new OutingsInquiry($request));

        $data = 'sent'; //view('master.about.contact.response')->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function contactForm(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:191',
            'email' => 'required|email',
            'input_comments' => 'required|max:500'
        ]);

        Mail::to(self::TO_MAIL)
            ->cc(self::CC_MAIL)
            ->bcc(self::BCC_MAIL)
            ->send(new ContactFormMessage($request));

        $data = 'sent'; //view('master.about.contact.response')->render();
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }
}
