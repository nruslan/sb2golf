<?php

namespace App\Http\Controllers\MvGolf;

use Illuminate\Http\Request;

class HomePageController extends BaseController
{
    protected $template;

    public function __construct()
    {
        parent::__construct();
        $this->template = "$this->template.home";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view("$this->template.index");
    }

    public function outings()
    {
        return view("$this->template.outings");
    }

    public function facilities()
    {
        return view("$this->template.facilities");
    }
}
