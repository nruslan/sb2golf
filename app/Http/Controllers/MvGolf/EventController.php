<?php

namespace App\Http\Controllers\MvGolf;

use App\Event;
use App\Http\Resources\Event as EventResource;

class EventController extends BaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $events = Event::with('picture')
            ->where('expired_at', '>', $this->nowDate())
            ->orderByDesc('created_at')
            ->paginate(3);

        return EventResource::collection($events);
    }
}
