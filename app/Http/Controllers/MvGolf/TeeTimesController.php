<?php

namespace App\Http\Controllers\MvGolf;

class TeeTimesController extends BaseController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('https://www.golf18network.com/arizona/tucson/mountain-view/golf/course/tee-times/7352.php?showall=1');
    }

    public function book()
    {
        return view('master.tee-times.book');
    }
}
