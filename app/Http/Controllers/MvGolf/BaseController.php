<?php

namespace App\Http\Controllers\MvGolf;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $template;

    public function __construct()
    {
        $this->template = 'master.mvgolf';
    }

    public function nowDate()
    {
        return Carbon::now();
    }
}
