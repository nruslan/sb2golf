<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Event extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'short_description' => $this->short_description,
            'day' => $this->day,
            'month' => $this->month,
            'date' => $this->date_time,
            'picture' => asset("storage/$this->event_picture"),
            'document' => asset("storage/$this->pdf_document")
        ];
    }
}
