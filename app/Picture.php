<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'filename', 'alternate_text', 'caption', 'folder_id', 'year'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function folder()
    {
        return $this->belongsTo('App\Folder', 'folder_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getPicPathAttribute()
    {
        return $this->folder_id
            ? $this->folder->path."/$this->year/$this->file_name"
            : 'folder-is-not-defined';
    }
}
