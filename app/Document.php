<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'filename', 'size', 'folder_id', 'year'
    ];

    public function folder()
    {
        return $this->belongsTo('App\Folder', 'folder_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getDocPathAttribute()
    {
        return $this->folder_id
            ? $this->folder->path."/$this->year/$this->filename"
            : 'na';
    }
}
