<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer(['layouts.app', 'master.sb2golf.home.index', 'master.sb2golf.contact.index'], function($view)
        {
            $email = "play@preservegolf.club";
            $phone = "(520) 825-9022";
            $tel = "520-825-9022";
            $clubName = "SaddleBrooke TWO Golf";

            $view->with([
                'email' => $email,
                'phone' => $phone,
                'tel' => $tel,
                'club_name' => $clubName
            ]);
        });

        // Public Service Providers
        view()->composer(['layouts.mvgolf', 'master.mvgolf.home.index', 'master.mvgolf.contact.index'], function($view)
        {
            $email = "play@mountainviewgolf.club";
            $phone = "(520) 818-1100";
            $tel = "520-818-1100";
            $clubName = "MountainView Golf Club";

            $view->with([
                'email' => $email,
                'phone' => $phone,
                'tel' => $tel,
                'club_name' => $clubName
            ]);
        });

        view()->composer(['layouts.preservegolf', 'master.preservegolf.home.index', 'master.preservegolf.contact.index'], function($view)
        {
            $email = "play@preservegolf.club";
            $phone = "(520) 825-9022";
            $tel = "520-825-9022";
            $clubName = "The Preserve Golf Club";

            $view->with([
                'email' => $email,
                'phone' => $phone,
                'tel' => $tel,
                'club_name' => $clubName
            ]);
        });

        view()->composer(['partials.annuals', 'partials.play-cards'], function($view)
        {
            $view->with([
                'dateRange' => '1/1/2020-12/31/2020'
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
