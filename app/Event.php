<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'picture_id', 'document_id', 'date_time', 'expired_at', 'title', 'subtitle', 'description'
    ];

    public function picture()
    {
        return $this->belongsTo('App\Picture', 'picture_id');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'events_locations', 'event_id', 'location_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------

    /**
     * Get Event's picture
     * @return string
     */
    public function getEventPictureAttribute()
    {
        return $this->picture_id
            ? $this->picture->pic_path
            : 'events/images/blank.jpg';
    }

    public function getShortDescriptionAttribute()
    {
        $start = 0;
        $strLength = 113;

        //Remove any titles <h1> - <h6>
        $no_title_txt = preg_replace('/<h[0-6][^>]*>([\s\S]*?)<\/h[0-6][^>]*>/', '', $this->description);

        // Remove HTML tags from the content
        $txt = strip_tags($no_title_txt);

        return strlen($txt) > $strLength
            ? substr($txt, $start, strrpos($txt, " ", -(strlen($txt) - $strLength)))
            : $txt;
    }

    public function getDayAttribute()
    {
        return date('d', strtotime($this->date_time));
    }

    public function getMonthAttribute()
    {
        return date('M', strtotime($this->date_time));
    }

    /**
     * Get PDF Document
     * @return string
     */
    public function getPdfDocumentAttribute()
    {
        return $this->document_id ? $this->document->doc_path : 'default.pdf';
    }
}
