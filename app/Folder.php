<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name', 'description'
    ];

    public function parentFolder()
    {
        return $this->belongsTo('App\Folder', 'parent_id');
    }

    // Accessors -------------------------------------------------------------------------------------------------------
    public function getPathAttribute()
    {
        return $this->parent_id
            ? $this->parentFolder->name."/$this->name"
            : $this->name;
    }
}
